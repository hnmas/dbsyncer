package org.dbsyncer.storage.binlog;

import org.dbsyncer.storage.binlog.proto.BinlogMessage;

import java.io.IOException;
import java.util.Queue;

/**
 * @author AE86
 * @version 1.0.0
 * @date 2022/6/8 23:34
 */
public interface BinlogRecorder {

    /**
     * 将任务序列化刷入磁盘
     *
     * @param message
     */
    void flush(BinlogMessage message) throws IOException;

    /**
     * 获取缓存队列
     *
     * @return
     */
    Queue getQueue();

    /**
     * 获取缓存队列容量
     *
     * @return
     */
    int getQueueCapacity();

}